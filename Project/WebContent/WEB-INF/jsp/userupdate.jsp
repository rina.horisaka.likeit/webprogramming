
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userupdate</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>


	<div class="p-3 mb-2 bg-secondary text-white">
		${userInfo.name} さん
		<p>
			<a href="LogoutServlet" class="text-danger">ログアウト</a>
		</p>
	</div>




	<h3>
		<div class="mx-auto" style="width: 200px;">ユーザ情報更新</div>
	</h3>


	<c:if test="${errMsg != null}">
		<p class="text-danger">${errMsg}</p>


	</c:if>


	<form class="form-signin" action="UserUpdateServlet" method="post">

		<div class="form-group row">
			<label for="staticLoginid" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-10">
				<input type="text" name="loginId" readonly
					class="form-control-plaintext" id="staticLoginid"
					value="${userList.loginId}">
			</div>
		</div>

		<div class="form-group row">
			<label for="inputPassword" class="col-sm-2 col-form-label">パスワード</label>
			<div class="col-sm-10">
				<input type="password" name="password" class="form-control"
					id="inputPassword" placeholder="●●●●●">
			</div>
		</div>

		<div class="form-group row">
			<label for="inputPasswordcon" class="col-sm-2 col-form-label">パスワード(確認)</label>
			<div class="col-sm-10">
				<input type="password" name="password2" class="form-control"
					id="inputPasswordcon" placeholder="●●●●●">
			</div>
		</div>

		<div class="form-group row">
			<label for="inputUsername" class="col-sm-2 col-form-label">ユーザ名</label>
			<div class="col-sm-10">
				<input type="text" name="userName" class="form-control"
					id="inputUsername" value="${userList.name}">
			</div>
		</div>

		<div class="form-group row">
			<label for="inputBirthday" class="col-sm-2 col-form-label">生年月日</label>
			<div class="col-sm-10">
				<input type="text" name="birthday" class="form-control"
					id="inputBirthday" value="${userList.birthDate}">
			</div>
		</div>







		<div class="mx-auto" style="width: 200px;">
			<button type="submit" class="btn btn-primary">更新</button>
		</div>
	</form>

	<h1></h1>


	<p>
		<a href="AllUserServlet" class="text-primary">戻る</a>
	</p>







</body>

</html>