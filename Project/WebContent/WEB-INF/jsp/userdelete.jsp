
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>userdelete</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>


	<div class="p-3 mb-2 bg-secondary text-white">${userInfo.name}
		さん
		<p>
			<a href="LogoutServlet" class="text-danger">ログアウト</a>
		</p>
	</div>



	<h3>
		<div class="mx-auto" style="width: 200px;">
			<br>ユーザ削除確認
		</div>
	</h3>


	<form class="form-signin" action="UserDeleteServlet" method="post">
		<br>
		<h6>ログインID : ${userList.loginId}</h6>

		<h6>を本当に削除してよろしいでしょうか。</h6>
		<input type="hidden" name="loginId" value="${userList.loginId}">

		<br></br>


		<div class="mx-auto" style="width: 200px;">

			<a href="AllUserServlet" class="btn btn-primary btn-lg">キャンセル</a>


			<button type="submit"  class="btn btn-secondary btn-lg">OK</button>

		</div>

	</form>

</body>

</html>