package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");
		if (userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String Id = request.getParameter("id");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		UserBeans userList = userDao.findUserDetails(Id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String userName = request.getParameter("userName");
		String birthday = request.getParameter("birthday");
		String createDate = request.getParameter("createDate");
		String updateDate = request.getParameter("updateDate");

		/** 更新に失敗した場合 **/

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();

		//パスワードとパスワード確認の入力内容が異なる場合
		//パスワード以外に未入力の項目がある場合

		if (!password.equals(password2) || (userName.equals("") || birthday.equals(""))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			Date birthDate = null;

			if (!birthday.equals("")) {
				birthDate = java.sql.Date.valueOf(birthday);
			}

			UserBeans user = new UserBeans();

			user.setLoginId(loginId);
			user.setName(userName);
			user.setBirthDate(birthDate);

			request.setAttribute("userList", user);

			// 入力画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;

		}

		/** 更新に成功した場合 **/
		//パスワードが両方空欄の場合
		if (password.equals("") && password2.equals("")) {

			userDao.updateRest(loginId, password, userName, birthday, createDate, updateDate);

		} else {

			String i=Password.pass(password);

			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			userDao.update(loginId, i, userName, birthday, createDate, updateDate);
		}
		// ユーザ一覧サーブレットにリダイレクト
		response.sendRedirect("AllUserServlet");

	}

}
