
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>login</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>

	<h2>
		<div class="mx-auto" style="width: 200px;">ログイン画面</div>
	</h2>


	<c:if test="${errMsg != null}">
		<p class="text-danger">${errMsg}</p>


	</c:if>

	<form class="form-signin" action="LoginServlet" method="post">



		<div class="form-group row">
			<label for="inputLoginid" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-10">
				<input type="text" name="loginId" class="form-control"
					id="inputLoginid" placeholder="id" required autofocus>
			</div>
		</div>

		<div class="form-group row">
			<label for="inputPassword" class="col-sm-2 col-form-label">パスワード</label>
			<div class="col-sm-10">
				<input type="password" name="password" class="form-control"
					id="inputPassword" placeholder="●●●●●" required>
			</div>
		</div>


		<div class="mx-auto" style="width: 200px;">
			<button type="submit" class="btn btn-primary">ログイン</button>
		</div>
	</form>

</body>

</html>