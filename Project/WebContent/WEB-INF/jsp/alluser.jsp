
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>alluser</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>


	<div class="p-3 mb-2 bg-secondary text-white">
		${userInfo.name} さん
		<p>
			<a href="LogoutServlet" class="text-danger">ログアウト</a>
		</p>


	</div>


	<h2>
		<div class="mx-auto" style="width: 200px;">ユーザ一覧</div>
	</h2>




	<p>
		<a href="userregisterServlet" class="text-primary">新規登録</a>
	</p>


	<form class="form-search" action="AllUserServlet" method="post">



		<div class="form-group row">
			<label for="inputLoginid" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-10">
				<input type="text" name="loginId" class="form-control"
					id="inputLoginid" placeholder="id"  >
			</div>
		</div>

		<div class="form-group row">
			<label for="inputUsername" class="col-sm-2 col-form-label">ユーザ名</label>
			<div class="col-sm-10">
				<input type="text" name="Username" class="form-control"
					id="inputUsername" placeholder="name" >
			</div>
		</div>

		<div class="form-group row">
			<label for="inputBirthday" class="col-sm-2 col-form-label">生年月日</label>


			<div class="row">
				<div class="col">
					<input type="date" name="Brithday"
						 class="form-control" placeholder="年/月/日" id="inputBirthday"
						 >
				</div>
				<h6>～</h6>
				<div class="col">
					<input type="date" name="Brithday2" class="form-control"
						placeholder="年/月/日"id="inputBirthday"  >
				</div>
			</div>



		</div>

		<div class="mx-auto" style="width: 200px;">
			<button type="submit" class="btn btn-primary">検索</button>
		</div>


	</form>



	<h1></h1>

	<table class="table table-bordered">
		<thead class="thead-light">
			<tr>
				<th scope="col">ログインID</th>
				<th scope="col">ユーザ名</th>
				<th scope="col">生年月日</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>


			<c:forEach var="user" items="${userList}">
				<tr>
					<th scope="row">${user.loginId}</th>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<td>
						<div class="mx-auto" style="width: 200px;">
							<a type="submit" class="btn btn-primary"
								href="UserDetailsServlet?id=${user.id}">詳細</a>

							<c:if test="${ userInfo.loginId == 'admin' || user.loginId == userInfo.loginId}">
								<a type="submit" class="btn btn-success"
									href="UserUpdateServlet?id=${user.id}">更新</a>
							</c:if>



							<c:if test="${ userInfo.loginId == 'admin'}">
								<a type="submit" class="btn btn-danger"
									href="UserDeleteServlet?id=${user.id}">削除</a>
							</c:if>

						</div>
					</td>
				</tr>
			</c:forEach>


		</tbody>
	</table>

</body>

</html>