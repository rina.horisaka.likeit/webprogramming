 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>userdetails</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>


    <div class="p-3 mb-2 bg-secondary text-white">${userInfo.name} さん
        <p><a href="LogoutServlet" class="text-danger">ログアウト</a></p>
    </div>



    <h5>
        <div class="mx-auto" style="width: 200px;">
            <strong>ユーザ情報詳細参照</strong>
        </div>
    </h5>


<form class="form-signin" action="UserDetailsServlet" method="post">



        <div class="form-group row">
            <label for="staticLoginid" class="col-sm-2 col-form-label"><strong>ログインID</strong></label>
            <div class="col-sm-10">
                <input type="text" name="loginId" readonly class="form-control-plaintext" id="staticLoginid" value="${userList.loginId}">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticUsername" class="col-sm-2 col-form-label"><strong>ユーザ名</strong></label>
            <div class="col-sm-10">
                <input type="text" name="name" readonly class="form-control-plaintext" id="staticUsername" value="${userList.name}">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticBirthday" class="col-sm-2 col-form-label"><strong>生年月日</strong></label>
            <div class="col-sm-10">
                <input type="text" name="birthDate" readonly class="form-control-plaintext" id="staticBirthday" value="${userList.birthDate}">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticRegisterday" class="col-sm-2 col-form-label"><strong>登録日時</strong></label>
            <div class="col-sm-10">
                <input type="text" name="createDate" readonly class="form-control-plaintext" id="staticRegisterday"
                    value="${userList.createDate}">
            </div>
        </div>

        <div class="form-group row">
            <label for="staticUpdateday" class="col-sm-2 col-form-label"><strong>更新日時</strong></label>
            <div class="col-sm-10">
                <input type="text"  name="updateDate" readonly class="form-control-plaintext" id="staticUpdateday"
                    value="${userList.updateDate}">
            </div>
        </div>

    </form>


    <p><a href="AllUserServlet" class="text-primary">戻る</a></p>


</body>

</html>