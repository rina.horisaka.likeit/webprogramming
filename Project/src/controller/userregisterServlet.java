package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class userregisterServlet
 */
@WebServlet("/userregisterServlet")
public class userregisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public userregisterServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");
		if (userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordcon = request.getParameter("passwordcon");
		String userName = request.getParameter("userName");
		String birthday = request.getParameter("birthday");
		String createDate = request.getParameter("createDate");
		String updateDate = request.getParameter("updateDate");

		/** 登録に失敗した場合 **/
		//既に登録されているログインIDが入力された場合

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		String user = userDao.findByLoginId(loginId);

		if (user != null) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// 入力画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//パスワードとパスワード確認の入力内容が異なる場合
		//入力項目に一つでも未入力のものがある場合
		if (!password.equals(passwordcon) || (loginId .equals("") || password  .equals("") ||
				passwordcon .equals("")|| userName  .equals("") || birthday .equals(""))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// 入力画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
			dispatcher.forward(request, response);
			return;
		}


		String i=Password.pass(password);

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		userDao.register(loginId, i, userName, birthday, createDate, updateDate);

		// ユーザ一覧サーブレットにリダイレクト
		response.sendRedirect("AllUserServlet");

	}

}
